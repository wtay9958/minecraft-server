package fr.etwin.generator.providers;

import fr.etwin.generator.KubeChunkProvider;
import fr.etwin.utils.KubeChunk;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileChunkProvider implements KubeChunkProvider {
    private final String file;

    public FileChunkProvider(String file) {
        this.file = file;
    }

    @Override
    public KubeChunk getChunk(int mX, int mY) {
        // For debug, all kube chunks will use the file.
        try {
            byte[] bytes = Files.readAllBytes(Paths.get(file));
            return new KubeChunk(mX, mY, bytes, false);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }
}
