import fr.etwin.generator.providers.FileChunkProvider;
import fr.etwin.utils.KubeBlock;
import fr.etwin.utils.KubeChunk;
import fr.etwin.generator.providers.DatabaseChunkProvider;
import fr.etwin.generator.KubeChunkProvider;
import fr.etwin.generator.providers.SocketChunkProvider;
import fr.etwin.utils.KubeConstants;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ConnectionTest {
    void verifChunk(KubeChunk chunk) {
        if (chunk == null) {
            fail("Chunk is null.");
        }
        if (chunk.data.length != KubeConstants.CHUNK_SIZE) {
            fail("Invalid chunk length");
        }
        if (chunk.getBlock(0,0,0) != KubeBlock.Water) {
            fail("Wrong data");
        }
    }

    @Test
    void connectionTest() {
        DatabaseChunkProvider kdb = new DatabaseChunkProvider();
        boolean success = kdb.connect();

        if (!success) {
            fail("Connection failed");
        }
        kdb.close();
    }

    @Test
    void getChunkDatabase() {
        KubeChunkProvider kdb = new DatabaseChunkProvider();
        KubeChunk chunk = kdb.getChunk(10,10);
        verifChunk(chunk);
    }

    @Test
    void getChunkFile() {
        KubeChunkProvider kdb = new FileChunkProvider("/srv/craftbukkit/plugins/KubePlugin/chunk.bin");
        KubeChunk chunk = kdb.getChunk(10,10);
        verifChunk(chunk);
    }

    @Test
    void getChunkSocket() {
        KubeChunkProvider kdb = new SocketChunkProvider(0);
        KubeChunk chunk = kdb.getChunk(10,10);
        verifChunk(chunk);
    }
}
